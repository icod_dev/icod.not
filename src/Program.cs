﻿using System.Linq;

namespace Icod.Not {

	public static class Program {

		#region nested classes
		private enum Mode {
			Undefined = 0,
			StartsWith,
			Contains,
			EndsWith
		}
		#endregion nested classes


		private const System.Int32 theBufferSize = 16384;

		[System.STAThread]
		public static System.Int32 Main( System.String[] args ) {
			if ( null == args ) {
				args = new System.String[ 0 ];
			}
			var len = args.Length;
			if ( (  len < 4 ) || ( 10 < len ) ) {
				PrintUsage();
				return 1;
			}
			len = len - 1;
			System.String inputPathName = null;
			System.String outputPathName = null;
			System.String @string = System.String.Empty;
			System.StringComparison compare = System.StringComparison.CurrentCulture;
			Mode mode = Mode.Undefined;
			System.String @switch = null;
			System.Int32 i = -1;
			do {
				@switch = args[ ++i ];
				if ( "--input".Equals( @switch, System.StringComparison.OrdinalIgnoreCase ) ) {
					inputPathName = args[ ++i ].TrimToNull();
				} else if ( "--output".Equals( @switch, System.StringComparison.OrdinalIgnoreCase ) ) {
					outputPathName = args[ ++i ].TrimToNull();
				} else if ( "--string".Equals( @switch, System.StringComparison.OrdinalIgnoreCase ) ) {
					@string = args[ ++i ];
				} else if ( "--mode".Equals( @switch, System.StringComparison.OrdinalIgnoreCase ) ) {
					mode = (Mode)System.Enum.Parse( typeof( Mode ), args[ ++i ].TrimToNull(), true );
				} else if ( "--compare".Equals( @switch, System.StringComparison.OrdinalIgnoreCase ) ) {
					compare = (System.StringComparison)System.Enum.Parse( typeof( System.StringComparison ), args[ ++i ].TrimToNull(), true );
				} else {
					PrintUsage();
					return 1;
				}
			} while ( i < len );

			System.Func<System.String, System.String, System.StringComparison, System.Boolean> worker = null;
			switch ( mode ) {
				case Mode.StartsWith:
					worker = ( a, b, c ) => a.StartsWith( b, c );
					break;
				case Mode.Contains:
					worker = ( a, b, c ) => ( 0 <= a.IndexOf( b, c ) );
					break;
				case Mode.EndsWith:
					worker = ( a, b, c ) => a.EndsWith( b, c );
					break;
				default:
					PrintUsage();
					return 1;
			}

			System.Func<System.String, System.Collections.Generic.IEnumerable<System.String>> reader = null;
			if ( System.String.IsNullOrEmpty( inputPathName ) ) {
				reader = a => ReadStdIn();
			} else {
				reader = a => ReadFile( a );
			}

			System.Action<System.String, System.Collections.Generic.IEnumerable<System.String>> writer = null;
			if ( System.String.IsNullOrEmpty( outputPathName ) ) {
				writer = ( a, b ) => WriteStdOut( b );
			} else {
				writer = ( a, b ) => WriteFile( a, b );
			}

			writer(
				outputPathName,
				reader( inputPathName ).Where(
					x => !worker( x, @string, compare )
				)
			);
			return 0;
		}

		private static void PrintUsage() {
			System.Console.Error.WriteLine( "No, no, no! Use it like this, Einstein:" );
			System.Console.Error.WriteLine( "Not.exe --help" );
			System.Console.Error.WriteLine( "Not.exe --string theString --mode (StartsWith | Contains | EndsWith) [--compare (CurrentCulture | CurrentCultureIgnoreCase | InvariantCulture | InvariantCultureIgnoreCase | Ordinal | OrdinalIgnoreCase)] [--input inputFilePathName] [--output outputFilePathName]" );
			System.Console.Error.WriteLine( "Not.exe suppresses lines of input that start with, end with, or contain the specified string." );
			System.Console.Error.WriteLine( "The default value for the --compare switch is CurrentCulture." );
			System.Console.Error.WriteLine( "inputFilePathName and outputFilePathName may be relative or absolute paths." );
			System.Console.Error.WriteLine( "If inputFilePathName is omitted then input is read from StdIn." );
			System.Console.Error.WriteLine( "If outputFilePathName is omitted then output is written to StdOut." );
		}

		#region io
		private static System.Collections.Generic.IEnumerable<System.String> ReadStdIn() {
			var line = System.Console.In.ReadLine();
			while ( null != line ) {
				line = line.TrimToNull();
				if ( null != line ) {
					yield return line;
				}
				line = System.Console.In.ReadLine();
			}
		}
		private static System.Collections.Generic.IEnumerable<System.String> ReadFile( System.String filePathName ) {
			filePathName = filePathName.TrimToNull();
			if ( System.String.IsNullOrEmpty( filePathName ) ) {
				throw new System.ArgumentNullException( "filePathName" );
			}
			using ( var file = System.IO.File.Open( filePathName, System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.Read ) ) {
				using ( var reader = new System.IO.StreamReader( file, System.Text.Encoding.UTF8, true, theBufferSize, true ) ) {
					var line = reader.ReadLine();
					while ( null != line ) {
						line = line.TrimToNull();
						if ( null != line ) {
							yield return line;
						}
						line = reader.ReadLine();
					}
				}
			}
		}

		private static void WriteStdOut( System.Collections.Generic.IEnumerable<System.String> data ) {
			foreach ( var datum in data ) {
				System.Console.Out.WriteLine( datum );
			}
		}
		private static void WriteFile( System.String filePathName, System.Collections.Generic.IEnumerable<System.String> data ) {
			filePathName = filePathName.TrimToNull();
			if ( System.String.IsNullOrEmpty( filePathName ) ) {
				throw new System.ArgumentNullException( "filePathName" );
			}
			using ( var file = System.IO.File.Open( filePathName, System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.Write, System.IO.FileShare.None ) ) {
				file.Seek( 0, System.IO.SeekOrigin.Begin );
				using ( var writer = new System.IO.StreamWriter( file, System.Text.Encoding.UTF8, theBufferSize, true ) ) {
					foreach ( var datum in data ) {
						writer.WriteLine( datum );
					}
					writer.Flush();
				}
				file.Flush();
				file.SetLength( file.Position );
			}
		}
		#endregion io

		private static System.String TrimToNull( this System.String @string ) {
			if ( System.String.IsNullOrEmpty( @string ) ) {
				return null;
			}
			@string = @string.Trim();
			return System.String.IsNullOrEmpty( @string )
				? null
				: @string
			;
		}

	}

}